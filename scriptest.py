import sys
import os
import time

if __name__ == '__main__':
    string = "Hello {} !".format(str(sys.argv))
    print(string)
    with open(os.path.join(os.getcwd(), "out" + str(time.time())), 'w') as f:
        f.write(string + "\n")
